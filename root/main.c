/**************************************************************************//**
 * @file
 * @brief template for agro modem
 * @version 1.0.0
 ******************************************************************************
 * @section License
 * <b>Copyright 2015 Silicon Labs, Inc. http://www.silabs.com</b>
 *******************************************************************************
 *
 * This file is licensed under the Silabs License Agreement. See the file
 * "Silabs_License_Agreement.txt" for details. Before using this software for
 * any purpose, you must agree to the terms of that agreement.
 *
 ******************************************************************************/

#include <stdint.h>
#include <stdbool.h>
#include <intrinsics.h>
#include "em_device.h"
#include "em_chip.h"
#include "em_emu.h"
#include "em_cmu.h"
#include "em_gpio.h"
#include "em_rtc.h"

#include "gpio.h"
#include "rtc.h"
#include "spi.h"
#include "radio.h"
#include "adc.h"
#include "uart.h"
#include "mem.h"

#define MEASUR_PERIOD 40
#define SEND_PERIOD 4000
#define POWER 9
#define ONE_SEC  (32768)
#define TWO_SEC  ((ONE_SEC*2)-1)
#define MSEC_500 ((ONE_SEC/2)-1)
#define MSEC_2 ((ONE_SEC/500)-1)

void sleepMode(void)
{
  //gpioPULSEPWROFF();
  
  RTC_IntClear(RTC_IFC_COMP0);
  RTC_IntDisable(RTC_IEN_COMP0 );
  NVIC_DisableIRQ(GPIO_ODD_IRQn);
  RTC_IntEnable(RTC_IEN_COMP0 );
  EMU_EnterEM2(true);
  
}

uint8_t data[SEND_PERIOD/MEASUR_PERIOD + 3];  //50 - data 1 - period 2 - crc
//uint8_t data[53];
uint8_t dataLen = sizeof(data);	
uint8_t adcData = 0;
receive_t RADIO_IN = {20,0,0};	//default period = 20mS, dataPtr = 0,lastSent=0,dataValid=0
uint32_t goodCnt = 0, badCnt = 0;

/**************************************************************************//**
 * @brief Update clock and wait in EM2 for RTC tick.
 *****************************************************************************/
void clockLoop(void)
{
  for(int i = 0; i < dataLen;i++)data[i] = i;
  //gpioLDOOn();
  radioTest(data, dataLen,POWER);
  //gpioPULSEPWRON();
  memTest();
  rtcWait(2000);
  radioRXCon();
  while (1)
  {
    //nextTx(data,dataLen,true);
//    gpioSetLED();
//    gpioRST(0);
//    rtcWait(6);
//    gpioRST(1);
//    radioTest(data, 5,POWER);
//    rtcWait(110);  
//    gpioClearLED();
//    rtcSetWakeUp(ONE_SEC*2);
//   gpioClearLED();
// radioSleep();
//    sleepMode();
    if(getRXDoneFlag())
    {
      if(radioReadRXBuf(&RADIO_IN))  //check message
      {
        goodCnt++;
        RADIO_IN.dataPtr = 0;  //start sending from the first element of receiving array
      }
      else badCnt++;
      gpioToggleLED(); 
      uartSend(RADIO_IN.buffer[RADIO_IN.dataPtr]);
      data[0] = RADIO_IN.buffer[RADIO_IN.dataPtr];
      rtcWait(500);
      radioTest(data, 5,POWER); //quotation
      rtcWait(500);
      radioRXCon();
    }
    //adcData = adcRumRead();
    //uartSend(adcData);
    rtcWait(20);
  }
}

/**************************************************************************//**
 * @brief  Main function
 *****************************************************************************/
int main(void)
{
  /* Chip errata */
  CHIP_Init();

  /* Ensure core frequency has been updated */
  SystemCoreClockUpdate();


  /* Setup RTC to generate an interrupt every minute */
  rtcSetup();  

  /* Setup GPIO interrupt to set the time */
  gpioSetup();
  
  spiSetup();
  gpioRST(0);
  gpioSetLED();
  rtcWait(200);
  gpioClearLED();
  gpioRST(1);
  
  adcInit();
  
  uartInit(9600);
  /* Main function loop */
  clockLoop();

  return 0;
}

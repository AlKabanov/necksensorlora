
/**************************************************************************//**
 * @brief Enables LFACLK and selects LFXO as clock source for RTC
 *        Sets up the RTC to generate an interrupt every minute.
 *****************************************************************************/
void rtcSetup(void);

void rtcWait(uint32_t Sec);

/*
*@brief sets time to wake up
*@parameters 32768 =  1 sec
*/
void rtcSetWakeUp(uint32_t value);